#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QClipboard>
#include <QStandardItemModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void clipboardChanged();
    void pasteButtonClicked();

private:
    Ui::MainWindow *ui;
    QStandardItemModel * listViewModel;

    QClipboard * clipboard;
    QStringList history;
};

#endif // MAINWINDOW_H

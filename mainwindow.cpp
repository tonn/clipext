#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include "windowsapi.h"
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    clipboard = QApplication::clipboard();

    connect(clipboard, SIGNAL(dataChanged()), this, SLOT(clipboardChanged()));

    connect(ui->pasteButton, SIGNAL(clicked()), this, SLOT(pasteButtonClicked()));

    listViewModel = new QStandardItemModel(this);

    ui->listView->setModel(listViewModel);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clipboardChanged()
{
    QString text = clipboard->text();
    history.append(text);
    listViewModel->appendRow(new QStandardItem(text));
}

void MainWindow::pasteButtonClicked()
{
    this->setWindowState(Qt::WindowMinimized);
    Paste();
}

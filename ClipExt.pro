#-------------------------------------------------
#
# Project created by QtCreator 2016-05-04T02:25:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClipExt
TEMPLATE = app

LIBS += -luser32

SOURCES += main.cpp\
        mainwindow.cpp \
        windowsapi.cpp

HEADERS  += mainwindow.h \
            windowsapi.h

FORMS    += mainwindow.ui
